#!/bin/sh

# configs
AUUID=ceb225da-c4b6-4ab1-a910-b55b1368b768
CADDYIndexPage=https://github.com/AYJCSGM/mikutap/archive/master.zip
CONFIGCADDY=https://gitlab.com/TyshawnWalksBosco1/saway/-/raw/main/etc/Caddyfile
CONFIGXRAY=https://gitlab.com/TyshawnWalksBosco1/saway/-/raw/main/etc/xray.json
ParameterSSENCYPT=chacha20-ietf-poly1305
StoreFiles=https://gitlab.com/TyshawnWalksBosco1/saway/-/raw/main/etc/StoreFiles
#PORT=4433
mkdir -p /etc/caddy/ /usr/share/caddy && echo -e "User-agent: *\nDisallow: /" >/usr/share/caddy/robots.txt
wget $CADDYIndexPage -O /usr/share/caddy/index.html && unzip -qo /usr/share/caddy/index.html -d /usr/share/caddy/ && mv /usr/share/caddy/*/* /usr/share/caddy/
wget -qO- $CONFIGCADDY | sed -e "1c :$PORT" -e "s/\$AUUID/$AUUID/g" -e "s/\$MYUUID-HASH/$(caddy hash-password --plaintext $AUUID)/g" >/etc/caddy/Caddyfile
wget -qO- $CONFIGXRAY | sed -e "s/\$AUUID/$AUUID/g" -e "s/\$ParameterSSENCYPT/$ParameterSSENCYPT/g" >/xray.json

# storefiles
mkdir -p /usr/share/caddy/$AUUID
wget -P /usr/share/caddy/$AUUID https://gitlab.com/TyshawnWalksBosco1/resiancosc/-/raw/main/resiancosc.zip


for file in $(ls /usr/share/caddy/$AUUID); do
    [[ "$file" != "StoreFiles" ]] && echo \<a href=\""$file"\" download\>$file\<\/a\>\<br\> >>/usr/share/caddy/$AUUID/ClickToDownloadStoreFiles.html
done

# start
tor &

/xray -config /xray.json &

caddy run --config /etc/caddy/Caddyfile --adapter caddyfile
